import React, {Component} from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import {connect} from 'react-redux'
import {TopPage} from './top.js'
import {JapaneseTopPage} from './japaneseTop.js'
import {Row, Col} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import {bindActionCreators} from 'redux'
import * as actions from './action/actions.js';

class App extends Component {

    constructor(props) {
        super(props);
        const {dispatch} = props
        this.action = bindActionCreators(actions, dispatch)
        this.state = ({
            language:"en"
        });
    }
    
    render() {

        const changeLanguageToJa = () =>{
            this.action.changeLanguageToJa()
            this.setState({language: "ja"})
        }

        const changeLanguageToEn = () =>{
            this.action.changeLanguageToEn()
            this.setState({language: "en"})
        }

        return(
            <Router>
                <Row className="fixed-top" > 
                    <Col>
                        <header>
                            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                                <Link to="/top" className="navbar-brand" onClick={changeLanguageToEn}><h2>TOM&apos;S WORLD NEWS</h2></Link>
                                <div className="collapse navbar-collapse" id="navbarNav">
                                    <ul className="navbar-nav">
                                    <li className="nav-item">
                                    {(this.props.language === "en") ? (
                                            <Link to="/JapaneseTop" className="nav-link" onClick={changeLanguageToJa}>Japanese</Link>
                                        ) : (
                                            <Link to="/top" className="nav-link" onClick={changeLanguageToEn}>English</Link>
                                    )}
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/top" className="nav-link" onClick={changeLanguageToEn}>HOME<span className="sr-only"></span></Link>
                                    </li>
                                    </ul>
                                </div>
                            </nav>
                        </header>
                    </Col>
                </Row>
                <Switch>
                    <Route exact path="/top" component={TopPage}></Route>
                    <Route path="/japaneseTop" component={JapaneseTopPage}></Route>
                </Switch>
            </Router>
        )
    }
}


function mapStateToProps(state){
    return state
}

App.propTypes = {
    dispatch: PropTypes.func,
    language: PropTypes.string,
    status: PropTypes.string,
    news:PropTypes.array,
    newsTranslateIntoJa:PropTypes.array
}

export default connect(mapStateToProps)(App)