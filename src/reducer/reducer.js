const defaultState = {
    language: "en",
    status: "translated",
    news:[],
    newsTranslateIntoJa:[]
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'UPDATE_NEWS':
            return {
                ...state,
                news: action.news,
            }
        case 'UPDATE_JAPANESE_NEWS':
            return {
                ...state,
                newsTranslateIntoJa: action.newsTranslateIntoJa,
            }

        case 'CHANGE_LANGUAGE':
            return{
                ...state,
                language: action.language
            }
        
        case 'CHANGE_STATUS':
            return{
                ...state,
                status: action.status
            }

        default:
            return state
    }
}