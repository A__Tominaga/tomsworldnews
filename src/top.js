import React, {Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import * as actions from './action/actions.js';
import {News} from './news.js'
import {Row, Col} from 'react-bootstrap'

class TopPage_Comp extends Component {

    constructor(props) {
        super(props);
        const {dispatch} = props
        this.action = bindActionCreators(actions, dispatch)
        this.state = ({
            language:"en"
        });
    }

    componentDidMount(){
        if(!this.props.news.length){
        // fetch("testNews.json")
            fetch("https://newsapi.org/v2/top-headlines?category=general&country=us&apiKey=ENTER YOUR API KEY")
            .then((response) => {
                response.json()
                .then(responseJson => {
                    if(responseJson.status === "ok"){
                        let news = responseJson.articles
                        this.action.updateNews(news)
                        this.action.changeStatusNotTranslated()
                        console.log('Using newsApi succeed.');
                        this.props.history.push("/top")
                    }
                })
            })
        }
    }

    render(){
        return(
            <div>
                <Row>
                    <Col md={{ span: 2.5, offset: 5 }}>
                        <h3 className="topic">TODAY&apos;S TOPIC</h3>
                    </Col>
                </Row>
                {this.newsList(this.props.news, this.state.language)}
            </div>
        )
    }

    newsList(list, language){
        const newsList = list.map((content, index) => {
        
          return (
              <News key={index} news={content} language={language}/>
            )
        })
        
        return <div className="content">{newsList}</div>
    }

}
TopPage_Comp.propTypes = {
    history:PropTypes.object,
    dispatch: PropTypes.func,
    status: PropTypes.string,
    language:PropTypes.string,
    news:PropTypes.array,
    newsTranslateIntoJa:PropTypes.array
}

function mapStateToProps(state) {
    return state
}

export const TopPage = withRouter(connect(mapStateToProps)(TopPage_Comp))