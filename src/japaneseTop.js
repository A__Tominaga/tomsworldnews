import React, {Component} from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import * as actions from './action/actions.js';
import {News} from './news.js'
import {Row, Col} from 'react-bootstrap'


class JapaneseTopPage_Comp extends Component {
    constructor(props) {
        super(props);
        const {dispatch} = props
        this.action = bindActionCreators(actions, dispatch)
        this.state = ({
          language:"ja"
        });
    }

    componentDidMount(){
        if(!this.props.news.length){
            // fetch("testNews.json")
            fetch("https://newsapi.org/v2/top-headlines?category=general&country=us&apiKey=ENTER YOUR API KEY")
            .then((response) => {
                response.json()
                .then(responseJson => {
                    let news = responseJson.articles
                    this.action.updateNews(news)
                    if(responseJson.status === "ok"){
                        let news = responseJson.articles
                        this.action.updateNews(news)
                        fetch("http://localhost:8080/translateToJa", {
                            method:"POST",
                            body:JSON.stringify(news)
                        })
                        .then((response) => {
                            response.json()
                            .then(responseJson => {
                                this.action.updateJapaneseNews(responseJson)
                            })
                            if(response.status === 200){
                                console.log("translate succeed!")
                                this.action.changeStatusTranslated()
                                this.action.changeLanguageToJa()
                                this.props.history.push("/JapaneseTop")
                            }
                        })
                        .catch(error => console.error(error))
                        console.log('Using newsApi in JapanesePage succeed.')
                    }
                })
            })
        }

        if(this.props.status === 'notTranslated'){
            fetch("http://localhost:8080/translateToJa", {
                method:"POST",
                body:JSON.stringify(this.props.news)
            })
            .then((response) => {
                response.json()
                .then(responseJson => {
                    this.action.updateJapaneseNews(responseJson)
                })
                if(response.status === 200){
                    console.log("translate succeed!")
                    this.action.changeStatusTranslated()
                    this.props.history.push("/JapaneseTop")
                }
            })
            .catch(error => console.error(error))
        }
    }

    render(){
        return(
            <div>
                <Row>
                    <Col md={{ span: 2.5, offset: 5 }}>
                        <h3 className="topic">トップニュース</h3>
                    </Col>
                </Row>
                {this.newsList(this.props.newsTranslateIntoJa, this.state.language)}
            </div>
        )
    }
    newsList(list, language){
        const newsList = list.map((content, index) => {
        
          return (
              <News key={index} news={content} language={language}/>
            )
        })
        
        return <div className="content">{newsList}</div>
    }
}

JapaneseTopPage_Comp.propTypes = {
    history:PropTypes.object,
    language:PropTypes.string,
    status: PropTypes.string,
    dispatch: PropTypes.func,
    news:PropTypes.array,
    newsTranslateIntoJa:PropTypes.array
}

function mapStateToProps(state) {
    return state
}

export const JapaneseTopPage =  withRouter(connect(mapStateToProps)(JapaneseTopPage_Comp))