import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Row, Col} from 'react-bootstrap'

export class News extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Row className="news">
                <Col md={{ span: 3, offset: 2 }}>
                    {(this.props.news.urlToImage === null) ? (
                        <img className="mx-auto d-block" src={"NoImage.png"} alt="NoImage..." width="100%" height="90%"></img>
                    ):(
                        <img className="mx-auto d-block" src={this.props.news.urlToImage} alt="NoImage..." width="100%" height="90%"></img>
                    )}
                </Col>
                <Col md={{ span: 5, offset: 0 }}>
                    {(this.props.news.title === null || this.props.news.title === "ヌル") ? (
                        (this.props.language === "en") ? (
                                <h4>Can&apos;t get title...</h4>
                            ):(
                                <h4>タイトルが取得できませんでした</h4>
                        )
                    ):(
                        <a href={this.props.news.url} target="blank">
                            <h4>{this.props.news.title}</h4>
                        </a>
                    )}

                    {(this.props.news.publishedAt === null) ? (
                        (this.props.language === "en") ? (
                                <p className="publishedAt">Can&apos;t get publish date...</p>
                            ):(
                                <p className="publishedAt">公開日が取得できませんでした</p>
                        )
                    ):(
                        (this.props.language === "en") ? (
                            <p className="publishedAt">At {new Date(this.props.news.publishedAt).toLocaleString('en-US', { timeZone: 'UTC'})}(UTC)</p>
                        ):(
                            <p className="publishedAt">公開日 {new Date(this.props.news.publishedAt).toLocaleString('ja-JP', { timeZone: 'JST' })}(JST)</p>
                        )
                    )}
                    {(this.props.news.description === null || this.props.news.description === "ヌル") ? (
                        (this.props.language === "en") ? (
                                <p className="description">Can&apos;t get description...</p>
                            ):(
                                <p className="description">記事が取得できませんでした</p>
                        )
                    ):(
                        <p className="description">{this.props.news.description}</p>
                    )}
                    {(this.props.news.author === null) ? (
                        (this.props.language === "en") ? (
                                <p className="author">Can&apos;t get author...</p>
                            ):(
                                <p className="author">筆者が取得できませんでした</p>
                        )
                    ):(
                        (this.props.language === "en") ? (
                            <p className="author">author : {this.props.news.author}</p>
                        ):(
                            <p className="author">筆者 : {this.props.news.author}</p>
                        )
                    )}
                    {(this.props.news.source.name === null) ? (
                        (this.props.language === "en") ? (
                                <p className="source">Can&apos;t get source...</p>
                            ):(
                                <p className="source">ソースが取得できませんでした</p>
                        )
                    ):(
                        (this.props.language === "en") ? (
                            <p className="source">source : {this.props.news.source.name}</p>
                        ):(
                            <p className="source">ソース : {this.props.news.source.name}</p>
                        )
                    )}
                    
                </Col>
            </Row>
        )
    }
}

News.propTypes = {
    language: PropTypes.string,
    news:PropTypes.exact({
        source:PropTypes.exact({
            id:PropTypes.string,
            name: PropTypes.string
        }),
        author: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        url: PropTypes.string,
        urlToImage: PropTypes.string,
        publishedAt: PropTypes.string,
        content:PropTypes.string
    })
}
