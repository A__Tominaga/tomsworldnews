export function updateNews(news) {
    return {
        type: 'UPDATE_NEWS',
        news: news
    }
}

export function updateJapaneseNews(newsTranslateIntoJa) {
    return {
        type: 'UPDATE_JAPANESE_NEWS',
        newsTranslateIntoJa: newsTranslateIntoJa
    }
}

export function changeLanguageToJa(){
    return{
        type: 'CHANGE_LANGUAGE',
        language: 'ja'
    }
}

export function changeLanguageToEn(){
    return{
        type: 'CHANGE_LANGUAGE',
        language: 'en'
    }
}

export function changeStatusTranslated(){
    return{
        type: 'CHANGE_STATUS',
        status: 'translated'
    }
}

export function changeStatusNotTranslated(){
    return{
        type: 'CHANGE_STATUS',
        status: 'notTranslated'
    }
}


export function getNews(){
    return {
        type: 'GET_NEWS'
    }
}